import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { User } from './user';
import { UserService } from './user.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
})
export class UsersComponent implements OnInit {
  users: User[];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService
      .getUsers()
      .pipe(
        tap((users) => {
          console.log('UsersComponent: tap 3');
          users.forEach((user) => console.log(user.fullName));
        })
      )
      // .pipe(tap((users) => (this.users = users)))
      .subscribe((users) => (this.users = users)); // this.users = users, lo usaremos en el tap
    // nos estamos suscribiendo si, pero sin hacer nada
    // .subscribe();
  }

  delete(user: User): void {
    const isDelete: boolean = window.confirm(
      `¿Estás seguro de eliminar el Cliente con el id '${user.id}'?`
    );
    if (isDelete) {
      this.userService.delete(user.id).subscribe((res) => {
        this.users = this.users.filter((usr) => usr.id !== user.id);
        swal.fire(
          'User eliminado!',
          `User ${user.fullName} eliminado con exito!`,
          'success'
        );
      });
    }
  }
}
