export class User {
  id: number;
  uid: string;
  username: string;
  email: string;
  password: string;
  rol: string;
  fullName: string;
  avatar: string;
}
