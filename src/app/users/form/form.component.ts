import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {
  user: User = new User();
  private title: string = 'Crear user';
  errors: string[];

  constructor(
    private userService: UserService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.loadUser();
  }

  loadUser(): void {
    this.activateRoute.params.subscribe((params) => {
      const id = params['id'];
      if (id) {
        this.userService.getUser(id).subscribe((user) => (this.user = user));
      }
    });
  }

  create(): void {
    this.userService.create(this.user).subscribe({
      next: (user) => {
        this.router.navigate(['/users']);
        swal.fire(
          'Nuevo usuario',
          `El usuario ${user.fullName} ha sido creado con exito`,
          'success'
        );
      },
      error: (err) => {
        this.errors = err.error.errors as string[];
        console.error('Codigo del error desde el backend: ' + err.status);
        console.error(err.error.errors);
      },
    });
  }

  update(): void {
    this.userService.update(this.user).subscribe({
      next: (json) => {
        this.router.navigate(['/users']);
        swal.fire(
          'Usuario actualizado',
          `${json.message}: ${json.user.fullName}`,
          'success'
        );
      },
      error: (err) => {
        this.errors = err.error.errors as string[];
        console.error('Codigo del error desde el backend: ' + err.status);
        console.error(err.error.errors);
      },
    });
  }
}
