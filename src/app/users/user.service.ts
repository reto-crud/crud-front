import { Injectable } from '@angular/core';
import { User } from './user';
import { map, catchError, throwError, Observable, of, tap } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private urlEndpoint: string = 'http://localhost:8080/users';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) {}

  getUsers(): Observable<User[]> {
    // return this.http.get<User[]>(this.urlEndpoint);
    // otra manera
    return this.http.get(this.urlEndpoint).pipe(
      tap((res) => {
        const users = res as User[];
        console.log('UserService: tap 1');
        users.forEach((user) => console.log(user.fullName));
      }),
      map((res) => {
        const users = res as User[];
        return users.map((user) => {
          user.fullName = user.fullName.toUpperCase();
          return user;
        });
      }),
      tap((res) => {
        console.log('UserService: tap 2');
        res.forEach((user) => console.log(user.fullName));
      })
    );
  }

  create(user: User): Observable<User> {
    return this.http
      .post(this.urlEndpoint, user, {
        headers: this.httpHeaders,
      })
      .pipe(
        map((json: any) => json.user as User),
        catchError((e) => {
          if (e.status === 400) {
            return throwError(() => e);
          }

          console.error(e.error.message);
          swal.fire(e.error.message, e.error.error, 'error');
          return throwError(() => e);
        })
      );
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.urlEndpoint}/${id}`).pipe(
      catchError((e) => {
        this.router.navigate(['/users']);
        console.error(e.error.message);
        swal.fire('Error al editar', e.error.message, 'error');
        return throwError(() => e);
      })
    );
  }
  //el user con  todos los datos pal update
  update(user: User): Observable<any> {
    return this.http
      .put<any>(`${this.urlEndpoint}/${user.id}`, user, {
        headers: this.httpHeaders,
      })
      .pipe(
        catchError((e) => {
          if (e.status === 400) {
            return throwError(() => e);
          }
          console.error(e.error.message);
          swal.fire(e.error.message, e.error.error, 'error');
          return throwError(() => e);
        })
      );
  }

  delete(id: number): Observable<User> {
    return this.http
      .delete<User>(`${this.urlEndpoint}/${id}`, {
        headers: this.httpHeaders,
      })
      .pipe(
        catchError((e) => {
          console.error(e.error.message);
          swal.fire(e.error.message, e.error.error, 'error');
          return throwError(() => e);
        })
      );
  }
}
